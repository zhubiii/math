<img width="40%" height="40%" src="http://mech-lang.org/img/logo.png">

Mech is a language for developing **data-driven**, **reactive** systems like animations, games, and robots. It makes **composing**, **transforming**, and **distributing** data easy, allowing you to focus on the essential complexity of your problem. 

Read about progress on our [blog](http://mech-lang.org/blog/), follow us on Twitter [@MechLang](https://twitter.com/MechLang), or join the mailing list: [talk@mech-lang.org](http://mech-lang.org/page/community/).

## Contents

Provided functions:

- math/atan
- math/ceil
- math/cos
- math/exp
- math/floor
- math/log
- math/round
- math/sin
- math/sqrt
- math/tan

## License

Apache 2.0